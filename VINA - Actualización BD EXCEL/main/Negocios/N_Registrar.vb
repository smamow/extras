﻿Public Class N_Registrar
    Private DD_registrar As D_Registrar

    Public Sub New()
        DD_registrar = New D_Registrar

    End Sub

    Function IRIS_WEBF_GRABA_EXCEL(
                                ByVal num As String,
                                ByVal codBarra As String,
                                ByVal establecimientoContenedor As String,
                                ByVal cajaTrans As String,
                                ByVal fechaIngreso As String,
                                ByVal muesRecep As String,
                                ByVal MuesEnv As String,
                                ByVal folioHojaTrabajo As String,
                                ByVal FechaEnvio As String,
                                ByVal fechaRecepcion As String,
                                ByVal fechaValidacion As String) As Integer

        Return DD_registrar.IRIS_WEBF_GRABA_EXCEL(
                                num,
                                codBarra,
                                establecimientoContenedor,
                                cajaTrans,
                                fechaIngreso,
                                muesRecep,
                                MuesEnv,
                                folioHojaTrabajo,
                                FechaEnvio,
                                fechaRecepcion,
                                fechaValidacion)

    End Function
End Class
