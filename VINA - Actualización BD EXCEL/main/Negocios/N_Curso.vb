﻿Public Class N_Curso
    Private DD_curso As D_Curso
    Private RE_DD_curso As D_Curso


    Public Sub New()
        DD_curso = New D_Curso
        RE_DD_curso = New D_Curso

    End Sub
    Function buscar_id_curso(ByVal curso As String) As List(Of E_Curso)
        Return DD_curso.Buscar_ID_curso(curso)
    End Function

    Function registrar_curso(ByVal curso As String) As List(Of E_Curso)
        Return RE_DD_curso.Registrar_Curso(curso)
    End Function
End Class


