﻿Public Class N_Año
    Private DD_Año As D_Año
    Private DD_RE_año As D_Año

    Public Sub New()
        DD_Año = New D_Año
        DD_RE_año = New D_Año

    End Sub
    Function buscar_ID_Año(ByVal año) As List(Of E_Año)

        Return DD_Año.Buscar_ID_Año(año)

    End Function

    Function Registar_Año(ByVal año) As List(Of E_Año)

        Return DD_RE_año.Registrar_Año(año)

    End Function

End Class
