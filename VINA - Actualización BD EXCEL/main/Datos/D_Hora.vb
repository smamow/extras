﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Hora
    Function Registrar_Hora(ByRef hora As String) As Integer

        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand

        Dim Read_Sql As Integer

        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_INSERT_HORA"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@HORA", OleDbType.VarChar).Value = hora
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        ' Leer datos entregados por la DB
        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql

    End Function
    Function Buscar_hora()
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Obj_Read_Dt As E_Hora
        Dim List_Reader As New List(Of E_Hora)
        Dim Reader_Comm As OleDbDataReader

        'Definiendo tipo de consulta a la BD
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_SEARCH_HORA"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Establecer conexion con la BD
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        'Leer datos entregados por la BD
        Reader_Comm = Cmd_command.ExecuteReader
        While Reader_Comm.Read
            Obj_Read_Dt = New E_Hora
            Obj_Read_Dt.ID_Hora = Reader_Comm(0)
            Obj_Read_Dt.A_B_HORA = Reader_Comm(1)
            List_Reader.Add(Obj_Read_Dt)

        End While

        Return List_Reader
    End Function
    Function Elimar_hora(ByVal id_hora As Integer) As Integer
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand

        Dim List_Reader As New List(Of E_Hora)
        Dim Read_Sql As Integer

        'Definiendo tipo de consulta a la BD
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_DELETE_HORA"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@ID_Año", OleDbType.SmallInt).Value = id_hora
        End With

        'Establecer conexion con la BD
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If
        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql
    End Function
End Class