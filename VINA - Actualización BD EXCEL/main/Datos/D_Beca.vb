﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Beca
    Function Registrar_Beca(ByRef beca As Integer, ByRef rut_beca As String) As Integer

        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand

        Dim Read_Sql As Integer

        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_UPDATE_DESCUENTO"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@RUT", OleDbType.VarChar).Value = rut_beca
            .Add("@BECA", OleDbType.Integer).Value = beca
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        ' Leer datos entregados por la DB
        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql

    End Function
End Class
