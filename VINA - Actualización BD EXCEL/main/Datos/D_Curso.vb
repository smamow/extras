﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Curso
    Function Buscar_ID_curso(ByVal curso As String) As List(Of E_Curso)

        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Obj_Read_Dt As E_Curso
        Dim List_Reader As New List(Of E_Curso)
        Dim Reader_Comm As OleDbDataReader

        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_RECIBIR_ID_CURSO"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@CURSO", OleDbType.VarChar).Value = curso
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        'Leer datos entregados por la DB
        Reader_Comm = Cmd_command.ExecuteReader
        While Reader_Comm.Read
            Obj_Read_Dt = New E_Curso
            Obj_Read_Dt.AÑ_ID_CURSO = Reader_Comm(0)
            List_Reader.Add(Obj_Read_Dt)
        End While

        Return List_Reader

    End Function

    Function Registrar_Curso(ByVal curso As String) As List(Of E_Curso)
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Obj_Read_Dt As E_Curso
        Dim List_Reader As New List(Of E_Curso)
        Dim Reader_Comm As OleDbDataReader

        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_INSERT_CURSO"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@CURSO", OleDbType.VarChar).Value = curso
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        'Leer datos entregados por la DB
        Reader_Comm = Cmd_command.ExecuteReader
        While Reader_Comm.Read
            Obj_Read_Dt = New E_Curso
            Obj_Read_Dt.RE_CURSO = Reader_Comm(0)
            List_Reader.Add(Obj_Read_Dt)
        End While

        Return List_Reader
    End Function
End Class
