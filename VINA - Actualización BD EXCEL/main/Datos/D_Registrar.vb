﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Registrar
    Function IRIS_WEBF_GRABA_EXCEL(ByVal num As String,
                                ByVal codBarra As String,
                                ByVal establecimientoContenedor As String,
                                ByVal cajaTrans As String,
                                ByVal fechaIngreso As String,
                                ByVal muesRecep As String,
                                ByVal MuesEnv As String,
                                ByVal folioHojaTrabajo As String,
                                ByVal FechaEnvio As String,
                                ByVal fechaRecepcion As String,
                                ByVal fechaValidacion As String) As Integer
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Read_Sql As Integer
        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_WEBF_GRABA_EXCEL"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@num", OleDbType.VarChar).Value = num
            .Add("@codBarra", OleDbType.VarChar).Value = codBarra
            .Add("@establecimientoContenedor", OleDbType.VarChar).Value = establecimientoContenedor
            .Add("@cajaTrans", OleDbType.VarChar).Value = cajaTrans
            .Add("@fechaIngreso", OleDbType.Date).Value = fechaIngreso
            .Add("@muesRecep", OleDbType.VarChar).Value = muesRecep
            .Add("@MuesEnv", OleDbType.VarChar).Value = MuesEnv
            .Add("@folioHojaTrabajo", OleDbType.VarChar).Value = folioHojaTrabajo
            .Add("@FechaEnvio", OleDbType.Date).Value = FechaEnvio
            .Add("@fechaRecepcion", OleDbType.Date).Value = fechaRecepcion
            .Add("@fechaValidacion", OleDbType.Date).Value = fechaValidacion

        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If
        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql
    End Function
End Class
