﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Resgistrar_Relacion
    Function actualizar(ByVal id_Año As String, ByVal id_Curso As String, ByVal id_Alumno As String) As Integer
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Read_Sql As Integer
        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_INSERT_IDS_INTO_RELACION"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@ID_AÑO", OleDbType.Integer).Value = id_Año
            .Add("@ID_CURSO", OleDbType.Integer).Value = id_Curso
            .Add("@ID_ALUMNO", OleDbType.Integer).Value = id_Alumno


        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If
        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql
    End Function
End Class
