﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Alumno
    Function Buscar_ID_alumno(ByRef rut As String) As List(Of E_Alumno)

        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Obj_Read_Dt As E_Alumno
        Dim List_Reader As New List(Of E_Alumno)
        Dim Reader_Comm As OleDbDataReader


        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_RECIBIR_ID_ALUMNO"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@RUT", OleDbType.VarChar).Value = rut
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        'Leer datos entregados por la DB
        Reader_Comm = Cmd_command.ExecuteReader
        While Reader_Comm.Read
            Obj_Read_Dt = New E_Alumno
            Obj_Read_Dt.AL_ID_ALUMNO = Reader_Comm(0)
            List_Reader.Add(Obj_Read_Dt)
        End While

        Return List_Reader

    End Function

    Function update_alumno(ByVal ID As String _
                           , ByVal rut As String _
                           , ByVal apellido_p As String _
                           , ByVal apellido_m As String _
                           , ByVal nombres As String _
                           , ByVal sexo As String _
                           , ByVal estado As String _
                           , ByVal apoderado As String _
                           , ByVal fono As String _
                           , ByVal email As String _
                           , ByVal descuento As Integer _
                           , ByVal options As Integer) As Integer
        'Declaraciones
        Dim obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Read_Sql As Integer

        'definiendo tipo de consulta a la BD
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_UPDATE_ALUMNO"
        Cmd_command.Connection = obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'enviar parametros
        With Cmd_command.Parameters
            .Add("@ID", OleDbType.Integer).Value = ID
            .Add("@RUT", OleDbType.VarChar).Value = rut
            .Add("@APELLIDO_PATERNO", OleDbType.VarChar).Value = apellido_p
            .Add("@APELLIDO_MATERNO", OleDbType.VarChar).Value = apellido_m
            .Add("@NOMBRES", OleDbType.VarChar).Value = nombres
            .Add("@SEXO", OleDbType.VarChar).Value = sexo
            .Add("@ESTADO", OleDbType.Integer).Value = estado
            .Add("@APODERADO", OleDbType.VarChar).Value = apoderado
            .Add("@FONO", OleDbType.VarChar).Value = fono
            .Add("@EMAIL", OleDbType.VarChar).Value = email
            .Add("@DESCUENTO", OleDbType.VarChar).Value = descuento
            .Add("@OPTION", OleDbType.Integer).Value = options

        End With

        'establece conexion con la base de datos
        If (obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            obj_Connect.Oledbconexion.Close()
        Else
            obj_Connect.Oledbconexion.Open()
        End If

        'ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql
    End Function

End Class
