﻿Imports System.Configuration
Imports System.Data.OleDb
Public Class D_Mensualidad
    Function insertarM(ByRef mensualidad As Integer, ByVal año As Integer, ByVal interes As Double) As Integer
        'Declaraciones
        Dim Obj_Connect As New Connection_BD
        Dim Cmd_command As New OleDb.OleDbCommand
        Dim Read_Sql As Integer
        'Definiendo tipo de consulta a la DB
        Cmd_command.CommandType = CommandType.StoredProcedure
        Cmd_command.CommandText = "IRIS_ANDALUE_INSERT_MENSUALIDAD"
        Cmd_command.Connection = Obj_Connect.CadenaConexion_IrisLab_LoBarnechea

        'Enviar Parámetros
        With Cmd_command.Parameters
            .Add("@MENSUALIDAD", OleDbType.Integer).Value = mensualidad
            .Add("@ID_AÑO", OleDbType.Integer).Value = año
            .Add("@INTERES", OleDbType.Double).Value = interes
        End With

        'Establecer conexión con la base de datos
        If (Obj_Connect.Oledbconexion.State = ConnectionState.Open) Then
            Obj_Connect.Oledbconexion.Close()
        Else
            Obj_Connect.Oledbconexion.Open()
        End If

        'Ejecutar PA y recibir un integer con la cantidad de filas afectadas
        Read_Sql = Cmd_command.ExecuteNonQuery
        Return Read_Sql

    End Function
End Class
