﻿Public Class Form1
    Dim id_Año As String
    Dim id_curso As String
    Dim id_alumno As String
    Dim contador As String
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LblAño.Text = Date.Now.Year

    End Sub
    Private Sub DataGridView1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.Control AndAlso e.KeyCode = Keys.V Then

            Try
                For Each line As String In Clipboard.GetText.Split(vbNewLine)
                    Dim item() As String = line.Trim.Split(vbTab)
                    If item.Length = Me.DataGridView1.ColumnCount Then
                        Me.DataGridView1.Rows.Add(item)
                    End If
                Next

            Catch ex As Exception
                MessageBox.Show(ex.Message, My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

    End Sub

    Private Sub BtnGuardar_Click_1(sender As Object, e As EventArgs) Handles BtnGuardar.Click

        Dim num As String
        Dim codBarra As String
        Dim establecimientoContenedor As String
        Dim cajaTrans As String
        Dim fechaIngreso As String
        Dim muesRecep As String
        Dim MuesEnv As String
        Dim folioHojaTrabajo As String
        Dim FechaEnvio As String
        Dim fechaRecepcion As String
        Dim fechaValidacion As String

        'Inicializar contador
        contador = 0

        For yy = 0 To DataGridView1.RowCount - 1
            num = Me.DataGridView1.Item(0, yy).Value
            codBarra = Me.DataGridView1.Item(1, yy).Value
            establecimientoContenedor = Me.DataGridView1.Item(2, yy).Value
            cajaTrans = Me.DataGridView1.Item(3, yy).Value
            fechaIngreso = Me.DataGridView1.Item(4, yy).Value
            muesRecep = Me.DataGridView1.Item(5, yy).Value
            MuesEnv = Me.DataGridView1.Item(6, yy).Value
            folioHojaTrabajo = Me.DataGridView1.Item(7, yy).Value
            FechaEnvio = Me.DataGridView1.Item(8, yy).Value
            fechaRecepcion = Me.DataGridView1.Item(9, yy).Value
            fechaValidacion = Me.DataGridView1.Item(10, yy).Value

            Dim NN_alumno As N_Registrar = New N_Registrar
            Dim retur As Integer

            retur = NN_alumno.IRIS_WEBF_GRABA_EXCEL(
                                num,
                                codBarra,
                                establecimientoContenedor,
                                cajaTrans,
                                fechaIngreso,
                                muesRecep,
                                MuesEnv,
                                folioHojaTrabajo,
                                FechaEnvio,
                                fechaRecepcion,
                                fechaValidacion)
            contador += retur

        Next yy
        MessageBox.Show("Filas Insertadas: " + contador, "Mensaje de Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click

        DataGridView1.Rows.Clear()
        id_curso = ""
        id_Año = ""
        id_alumno = ""

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
