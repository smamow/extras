﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu))
        Me.Button1 = New System.Windows.Forms.PictureBox()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.LOGO = New System.Windows.Forms.PictureBox()
        Me.Lbl5 = New System.Windows.Forms.Label()
        Me.lbl4 = New System.Windows.Forms.Label()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.lbl6 = New System.Windows.Forms.Label()
        Me.Lbl2 = New System.Windows.Forms.Label()
        CType(Me.Button1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LOGO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(624, 274)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 116)
        Me.Button1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Button1.TabIndex = 10
        Me.Button1.TabStop = False
        '
        'lbl1
        '
        Me.lbl1.AutoSize = True
        Me.lbl1.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl1.Location = New System.Drawing.Point(619, 206)
        Me.lbl1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(144, 29)
        Me.lbl1.TabIndex = 14
        Me.lbl1.Text = "Agregar Excel"
        '
        'LOGO
        '
        Me.LOGO.Image = CType(resources.GetObject("LOGO.Image"), System.Drawing.Image)
        Me.LOGO.InitialImage = CType(resources.GetObject("LOGO.InitialImage"), System.Drawing.Image)
        Me.LOGO.Location = New System.Drawing.Point(244, 15)
        Me.LOGO.Margin = New System.Windows.Forms.Padding(4)
        Me.LOGO.Name = "LOGO"
        Me.LOGO.Size = New System.Drawing.Size(221, 110)
        Me.LOGO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LOGO.TabIndex = 20
        Me.LOGO.TabStop = False
        '
        'Lbl5
        '
        Me.Lbl5.AutoSize = True
        Me.Lbl5.Font = New System.Drawing.Font("Cooper Black", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Lbl5.Location = New System.Drawing.Point(599, 41)
        Me.Lbl5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl5.Name = "Lbl5"
        Me.Lbl5.Size = New System.Drawing.Size(179, 54)
        Me.Lbl5.TabIndex = 21
        Me.Lbl5.Text = "CMVM"
        '
        'lbl4
        '
        Me.lbl4.AutoSize = True
        Me.lbl4.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.lbl4.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lbl4.Location = New System.Drawing.Point(795, 372)
        Me.lbl4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl4.Name = "lbl4"
        Me.lbl4.Size = New System.Drawing.Size(0, 29)
        Me.lbl4.TabIndex = 17
        '
        'lbl3
        '
        Me.lbl3.AutoSize = True
        Me.lbl3.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.lbl3.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lbl3.Location = New System.Drawing.Point(532, 372)
        Me.lbl3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(0, 29)
        Me.lbl3.TabIndex = 16
        '
        'lbl6
        '
        Me.lbl6.AutoSize = True
        Me.lbl6.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.lbl6.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.lbl6.Location = New System.Drawing.Point(809, 165)
        Me.lbl6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl6.Name = "lbl6"
        Me.lbl6.Size = New System.Drawing.Size(0, 29)
        Me.lbl6.TabIndex = 23
        '
        'Lbl2
        '
        Me.Lbl2.AutoSize = True
        Me.Lbl2.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.Lbl2.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Lbl2.Location = New System.Drawing.Point(619, 165)
        Me.Lbl2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Lbl2.Name = "Lbl2"
        Me.Lbl2.Size = New System.Drawing.Size(0, 29)
        Me.Lbl2.TabIndex = 15
        '
        'Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(177, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1316, 644)
        Me.Controls.Add(Me.lbl6)
        Me.Controls.Add(Me.Lbl5)
        Me.Controls.Add(Me.LOGO)
        Me.Controls.Add(Me.lbl4)
        Me.Controls.Add(Me.lbl3)
        Me.Controls.Add(Me.Lbl2)
        Me.Controls.Add(Me.lbl1)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximumSize = New System.Drawing.Size(1334, 691)
        Me.MinimumSize = New System.Drawing.Size(1334, 691)
        Me.Name = "Menu"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Menu"
        CType(Me.Button1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LOGO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As PictureBox
    Friend WithEvents lbl1 As Label
    Friend WithEvents LOGO As PictureBox
    Friend WithEvents Lbl5 As Label
    Friend WithEvents lbl4 As Label
    Friend WithEvents lbl3 As Label
    Friend WithEvents lbl6 As Label
    Friend WithEvents Lbl2 As Label
End Class
