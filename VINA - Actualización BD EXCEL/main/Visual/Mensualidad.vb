﻿Public Class Mensualidad
    Private Sub Mensualidad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lbl_año.Text = Date.Now.Year
    End Sub
    Private Sub textbox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtMensualidad.KeyPress
        If Not IsNumeric(e.KeyChar) Then
    
            e.Handled = True
        End If
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim mensualidad As Integer
        Dim interes As Double
        Dim rr As Integer
        Dim id_Año As String
        id_Año = ""

        Dim NN_mensualidad As N_Mensualidad = New N_Mensualidad
        Dim NN_interes As N_Mensualidad = New N_Mensualidad
        Try

            mensualidad = TxtMensualidad.Text
            interes = TxtInteres.Text

            Dim NN_Año As N_Año = New N_Año
            Dim Data_Año As List(Of E_Año)

            Data_Año = NN_Año.buscar_ID_Año(Date.Now.Year)

            For XX = 0 To (Data_Año.Count - 1)
                id_Año = Data_Año(XX).AÑ_ID_AÑO
            Next XX

            rr = NN_mensualidad.insertarM(mensualidad, id_Año, interes)
            If (rr <> 1) Then
                MsgBox("Datos insertados correctamente")
            End If
        Catch ex As Exception
            MsgBox("Datos NO ingresados")
        End Try

    End Sub

End Class