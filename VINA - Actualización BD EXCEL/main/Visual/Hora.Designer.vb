﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Hora
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Hora))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtHora = New System.Windows.Forms.TextBox()
        Me.lblaño = New System.Windows.Forms.Label()
        Me.lbl_hora = New System.Windows.Forms.Label()
        Me.btneliminar = New System.Windows.Forms.Button()
        Me.Dgvhora = New System.Windows.Forms.DataGridView()
        Me.hh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.Dgvhora, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(546, 304)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtHora
        '
        Me.txtHora.Location = New System.Drawing.Point(533, 253)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.Size = New System.Drawing.Size(100, 20)
        Me.txtHora.TabIndex = 4
        '
        'lblaño
        '
        Me.lblaño.AutoSize = True
        Me.lblaño.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.lblaño.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblaño.Location = New System.Drawing.Point(519, 206)
        Me.lblaño.Name = "lblaño"
        Me.lblaño.Size = New System.Drawing.Size(124, 22)
        Me.lblaño.TabIndex = 3
        Me.lblaño.Text = "Igresar hora"
        '
        'lbl_hora
        '
        Me.lbl_hora.AutoSize = True
        Me.lbl_hora.Font = New System.Drawing.Font("Arial", 12.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_hora.ForeColor = System.Drawing.Color.Silver
        Me.lbl_hora.Location = New System.Drawing.Point(639, 252)
        Me.lbl_hora.Name = "lbl_hora"
        Me.lbl_hora.Size = New System.Drawing.Size(72, 18)
        Me.lbl_hora.TabIndex = 21
        Me.lbl_hora.Text = "00:00:00"
        Me.lbl_hora.Visible = False
        '
        'btneliminar
        '
        Me.btneliminar.Location = New System.Drawing.Point(260, 373)
        Me.btneliminar.Name = "btneliminar"
        Me.btneliminar.Size = New System.Drawing.Size(143, 23)
        Me.btneliminar.TabIndex = 31
        Me.btneliminar.Text = "Eliminar hora"
        Me.btneliminar.UseVisualStyleBackColor = True
        '
        'Dgvhora
        '
        Me.Dgvhora.AllowUserToAddRows = False
        Me.Dgvhora.AllowUserToDeleteRows = False
        Me.Dgvhora.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Dgvhora.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgvhora.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.hh, Me.idH})
        Me.Dgvhora.Location = New System.Drawing.Point(260, 206)
        Me.Dgvhora.Name = "Dgvhora"
        Me.Dgvhora.ReadOnly = True
        Me.Dgvhora.Size = New System.Drawing.Size(143, 161)
        Me.Dgvhora.TabIndex = 30
        '
        'hh
        '
        DataGridViewCellStyle2.Format = "T"
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.hh.DefaultCellStyle = DataGridViewCellStyle2
        Me.hh.HeaderText = "Hora"
        Me.hh.Name = "hh"
        Me.hh.ReadOnly = True
        Me.hh.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'idH
        '
        Me.idH.HeaderText = "Id"
        Me.idH.Name = "idH"
        Me.idH.ReadOnly = True
        Me.idH.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(256, 162)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(149, 22)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Horas Actuales"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Cooper Black", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.label5.Location = New System.Drawing.Point(293, 23)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(407, 42)
        Me.label5.TabIndex = 33
        Me.label5.Text = "COLEGIO ANDALUE"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.InitialImage = CType(resources.GetObject("PictureBox2.InitialImage"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(123, 104)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 34
        Me.PictureBox2.TabStop = False
        '
        'Hora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(97, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1005, 570)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btneliminar)
        Me.Controls.Add(Me.Dgvhora)
        Me.Controls.Add(Me.lbl_hora)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtHora)
        Me.Controls.Add(Me.lblaño)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Hora"
        Me.Text = "Hora"
        CType(Me.Dgvhora, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents txtHora As TextBox
    Friend WithEvents lblaño As Label
    Friend WithEvents lbl_hora As Label
    Friend WithEvents btneliminar As Button
    Friend WithEvents Dgvhora As DataGridView
    Friend WithEvents idH As DataGridViewTextBoxColumn
    Friend WithEvents hh As DataGridViewTextBoxColumn
    Friend WithEvents Label1 As Label
    Friend WithEvents label5 As Label
    Friend WithEvents PictureBox2 As PictureBox
End Class
