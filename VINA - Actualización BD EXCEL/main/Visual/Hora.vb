﻿Public Class Hora
    Dim inhora As N_Hora = New N_Hora
    Dim Data_hora As List(Of E_Hora)
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Format_X As String
        Dim Hora_X As String
        Hora_X = ""
        Format_X = ""

        Try


            Hora_X = txtHora.Text

            Select Case Len(Hora_X)
                Case 8
                    Format_X = "##:##:##"
                Case Else
                    Format_X = "#:##:##"
            End Select
            If ((Hora_X Like Format_X) = True) Then

                If (IsDate(Hora_X) = True) Then
                    inhora.Registar_Hora(Hora_X)
                    MsgBox("Hora insertada")
                    txtHora.Text = ""
                    buscar_hora()
                Else
                    GoTo Err_hora
                End If
            Else
Err_hora:
                lbl_hora.Visible = True
                MsgBox("El formato ingresado no es correcto")
                txtHora.Text = ""

            End If
        Catch ex As Exception
            MsgBox("Ningun campo encontrado")
        End Try

    End Sub

    Private Sub Hora_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        buscar_hora()
    End Sub

    Private Sub btneliminar_Click(sender As Object, e As EventArgs) Handles btneliminar.Click
        Dim c_cell As Integer
        c_cell = 0

        c_cell = Dgvhora.CurrentCell.RowIndex

        Dim retorno As Integer

        retorno = inhora.eliminarhora(Dgvhora.Item(1, c_cell).Value)
        If (retorno = 1) Then
            Dgvhora.Rows.Clear()
            buscar_hora()
            MsgBox("Hora eliminada correctamente")



        Else
            buscar_hora()
            MsgBox("Error al eliminar hora")


        End If

    End Sub


    Private Sub buscar_hora()
        Dim NN_Hora As N_Hora = New N_Hora

        Data_hora = NN_Hora.buscar_hora()

        Dgvhora.RowCount = Data_hora.Count + 1

        If (Data_hora.Count > 0) Then ' se recorre la lista de hora que se busco en la base de datos
            For yy = 0 To (Data_hora.Count - 1)
                Dgvhora.Item(0, yy).Value = Data_hora(yy).A_B_HORA

                Dgvhora.Item(1, yy).Value = Data_hora(yy).ID_Hora

            Next yy

        End If

    End Sub


End Class