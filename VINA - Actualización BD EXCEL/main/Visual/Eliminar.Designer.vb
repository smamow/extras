﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Eliminar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Eliminar))
        Me.Lbl5 = New System.Windows.Forms.Label()
        Me.LOGO = New System.Windows.Forms.PictureBox()
        Me.BtnRut = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CbBuscar = New System.Windows.Forms.ComboBox()
        Me.DGVAlumno = New System.Windows.Forms.DataGridView()
        Me.Rut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Apellido_materno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Curso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nom_Apoderado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fono_Apoderado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Txt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Lblerror = New System.Windows.Forms.Label()
        CType(Me.LOGO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnRut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVAlumno, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Lbl5
        '
        Me.Lbl5.AutoSize = True
        Me.Lbl5.Font = New System.Drawing.Font("Cooper Black", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Lbl5.Location = New System.Drawing.Point(315, 23)
        Me.Lbl5.Name = "Lbl5"
        Me.Lbl5.Size = New System.Drawing.Size(407, 42)
        Me.Lbl5.TabIndex = 23
        Me.Lbl5.Text = "COLEGIO ANDALUE"
        '
        'LOGO
        '
        Me.LOGO.Image = CType(resources.GetObject("LOGO.Image"), System.Drawing.Image)
        Me.LOGO.InitialImage = CType(resources.GetObject("LOGO.InitialImage"), System.Drawing.Image)
        Me.LOGO.Location = New System.Drawing.Point(221, 8)
        Me.LOGO.Name = "LOGO"
        Me.LOGO.Size = New System.Drawing.Size(88, 74)
        Me.LOGO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LOGO.TabIndex = 22
        Me.LOGO.TabStop = False
        '
        'BtnRut
        '
        Me.BtnRut.BackColor = System.Drawing.Color.Transparent
        Me.BtnRut.Image = CType(resources.GetObject("BtnRut.Image"), System.Drawing.Image)
        Me.BtnRut.Location = New System.Drawing.Point(458, 121)
        Me.BtnRut.Name = "BtnRut"
        Me.BtnRut.Size = New System.Drawing.Size(20, 20)
        Me.BtnRut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.BtnRut.TabIndex = 26
        Me.BtnRut.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label2.Location = New System.Drawing.Point(66, 119)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 19)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Buscar:"
        '
        'CbBuscar
        '
        Me.CbBuscar.FormattingEnabled = True
        Me.CbBuscar.Items.AddRange(New Object() {"Rut", "Nombre", "Apellido paterno"})
        Me.CbBuscar.Location = New System.Drawing.Point(141, 120)
        Me.CbBuscar.Name = "CbBuscar"
        Me.CbBuscar.Size = New System.Drawing.Size(121, 21)
        Me.CbBuscar.TabIndex = 28
        '
        'DGVAlumno
        '
        Me.DGVAlumno.AllowUserToAddRows = False
        Me.DGVAlumno.AllowUserToDeleteRows = False
        Me.DGVAlumno.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.DGVAlumno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVAlumno.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Rut, Me.Apellido_Paterno, Me.Apellido_materno, Me.Nombres, Me.Sexo, Me.Curso, Me.Estado, Me.Nom_Apoderado, Me.Fono_Apoderado, Me.Email})
        Me.DGVAlumno.Location = New System.Drawing.Point(12, 190)
        Me.DGVAlumno.Name = "DGVAlumno"
        Me.DGVAlumno.ReadOnly = True
        Me.DGVAlumno.Size = New System.Drawing.Size(947, 204)
        Me.DGVAlumno.TabIndex = 29
        '
        'Rut
        '
        Me.Rut.HeaderText = "Rut"
        Me.Rut.Name = "Rut"
        Me.Rut.ReadOnly = True
        '
        'Apellido_Paterno
        '
        Me.Apellido_Paterno.HeaderText = "Apellido Paterno"
        Me.Apellido_Paterno.Name = "Apellido_Paterno"
        Me.Apellido_Paterno.ReadOnly = True
        '
        'Apellido_materno
        '
        Me.Apellido_materno.HeaderText = "Apellido Materno"
        Me.Apellido_materno.Name = "Apellido_materno"
        Me.Apellido_materno.ReadOnly = True
        '
        'Nombres
        '
        Me.Nombres.HeaderText = "Nombres"
        Me.Nombres.Name = "Nombres"
        Me.Nombres.ReadOnly = True
        '
        'Sexo
        '
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        '
        'Curso
        '
        Me.Curso.HeaderText = "Curso"
        Me.Curso.Name = "Curso"
        Me.Curso.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'Nom_Apoderado
        '
        Me.Nom_Apoderado.HeaderText = "Nombre Apoderado"
        Me.Nom_Apoderado.Name = "Nom_Apoderado"
        Me.Nom_Apoderado.ReadOnly = True
        '
        'Fono_Apoderado
        '
        Me.Fono_Apoderado.HeaderText = "Fono"
        Me.Fono_Apoderado.Name = "Fono_Apoderado"
        Me.Fono_Apoderado.ReadOnly = True
        '
        'Email
        '
        Me.Email.HeaderText = "Email"
        Me.Email.Name = "Email"
        Me.Email.ReadOnly = True
        '
        'Txt
        '
        Me.Txt.Location = New System.Drawing.Point(352, 121)
        Me.Txt.Name = "Txt"
        Me.Txt.Size = New System.Drawing.Size(100, 20)
        Me.Txt.TabIndex = 30
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label1.Location = New System.Drawing.Point(268, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 19)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Ingresar:"
        '
        'Lblerror
        '
        Me.Lblerror.AutoSize = True
        Me.Lblerror.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lblerror.ForeColor = System.Drawing.Color.LightGray
        Me.Lblerror.Location = New System.Drawing.Point(349, 144)
        Me.Lblerror.Name = "Lblerror"
        Me.Lblerror.Size = New System.Drawing.Size(0, 16)
        Me.Lblerror.TabIndex = 32
        '
        'Eliminar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(97, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1005, 570)
        Me.Controls.Add(Me.Lblerror)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Txt)
        Me.Controls.Add(Me.DGVAlumno)
        Me.Controls.Add(Me.CbBuscar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BtnRut)
        Me.Controls.Add(Me.Lbl5)
        Me.Controls.Add(Me.LOGO)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1005, 570)
        Me.MinimumSize = New System.Drawing.Size(1005, 570)
        Me.Name = "Eliminar"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Eliminar"
        CType(Me.LOGO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnRut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVAlumno, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Lbl5 As Label
    Friend WithEvents LOGO As PictureBox
    Friend WithEvents BtnRut As PictureBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CbBuscar As ComboBox
    Friend WithEvents DGVAlumno As DataGridView
    Friend WithEvents Rut As DataGridViewTextBoxColumn
    Friend WithEvents Apellido_Paterno As DataGridViewTextBoxColumn
    Friend WithEvents Apellido_materno As DataGridViewTextBoxColumn
    Friend WithEvents Nombres As DataGridViewTextBoxColumn
    Friend WithEvents Sexo As DataGridViewTextBoxColumn
    Friend WithEvents Curso As DataGridViewTextBoxColumn
    Friend WithEvents Estado As DataGridViewTextBoxColumn
    Friend WithEvents Nom_Apoderado As DataGridViewTextBoxColumn
    Friend WithEvents Fono_Apoderado As DataGridViewTextBoxColumn
    Friend WithEvents Email As DataGridViewTextBoxColumn
    Friend WithEvents Txt As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Lblerror As Label
End Class
