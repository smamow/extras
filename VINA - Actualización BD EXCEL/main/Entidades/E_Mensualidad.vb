﻿Public Class E_Mensualidad
    Private ID_MENSUALIDAD As String
    Private INTERES As String

    Public Property M_ID_MENSUALIDAD() As String
        Get
            Return ID_MENSUALIDAD
        End Get
        Set(ByVal value As String)
            ID_MENSUALIDAD = value
        End Set
    End Property

    Public Property M_INTERES() As String
        Get
            Return INTERES
        End Get
        Set(ByVal value As String)
            INTERES = value
        End Set
    End Property

End Class
