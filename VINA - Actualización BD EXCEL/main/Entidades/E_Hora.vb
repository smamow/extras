﻿Public Class E_Hora
    Private a_ID_Hora As String
    Private A_HORA As Date
    Public Property A_B_HORA() As Date
        Get
            Return A_HORA
        End Get
        Set(ByVal value As Date)
            A_HORA = value
        End Set
    End Property
    Public Property ID_Hora() As String
        Get
            Return a_ID_Hora
        End Get
        Set(ByVal value As String)
            a_ID_Hora = value
        End Set
    End Property
End Class
