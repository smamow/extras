﻿Public Class E_Año
    Private A_ID_AÑO As String
    Private A_RE_AÑO As String
    Public Property AÑ_ID_AÑO() As String
        Get
            Return A_ID_AÑO
        End Get
        Set(ByVal value As String)
            A_ID_AÑO = value
        End Set
    End Property

    Public Property AÑ_RE_AÑO() As String
        Get
            Return A_RE_AÑO
        End Get
        Set(ByVal value As String)
            A_RE_AÑO = value
        End Set
    End Property

End Class
